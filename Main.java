//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.

//<------- Nadipineni Bhargav ----->

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

class Task{
    private String text;
    private char status;
    private Date date;

    Task(String text, char status, Date date){
        this.text = text;
        this.status = status;
        this.date = date;
    }

    public void modify(){
        status  = (status=='p') ? ('c') : ('p');
    }

    public String getText(){
        return text;
    }
    public char getStatus(){
        return status;
    }

    public Date getDate(){
        return date;
    }
}


public class Main {

    static int size=0;
    static List<Task> Todolist = new ArrayList<>();
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        boolean flag=true;
        displayOptions();
        while(flag){
            System.out.println("Enter Choice :");
            if(scanner.hasNextInt()) {
                int choice = scanner.nextInt();
                if (choice == 1) {
                    FetchTodolist();
                } else if (choice == 2) {
                    CreateTask();
                } else if (choice == 3) {
                    ModifyTask();
                } else if (choice == 4) {
                    DeleteTask();
                } else if (choice == 5) {
                    SearchTask();
                }
                else if (choice == 6) {
                    displayOptions();
                }
                else if (choice == 7) {
                    flag = false;

                } else {
                    System.out.println("Invalid choice. Please enter a valid option.");
                }
            }else {
                System.out.println("Invalid input. Please enter a number.");
                scanner.next(); // Clear the invalid input from the scanner buffer
            }
        }
        scanner.close();
    }

    private static void displayOptions() {
        System.out.println("<---------- Welcome to the Todolist ---------->");
        System.out.println("1. Fetch TodoList");
        System.out.println("2. Add Task");
        System.out.println("3. Check the Task as Complete/Pending");
        System.out.println("4. Delete The Task");
        System.out.println("5. Search Task");
        System.out.println("6. Display Options");
        System.out.println("7. Terminate the Programme");
    }

    private static void SearchTask() {
        System.out.println("Enter the Text to Search");
        scanner.nextLine();
        String str = scanner.nextLine();
        System.out.println("Search Results :");
        boolean flag=false;
        for(int i=0;i<Todolist.size();i++){
            if(Todolist.get(i).getText().startsWith(str)){
                flag = true;
                System.out.println(i + ". " + Todolist.get(i).getText() + " - " + Todolist.get(i).getStatus() + " - " + Todolist.get(i).getDate());
            }
        }
        if(!flag){
            System.out.println("Nothing to Show");
        }

    }

    private static void DeleteTask() {
        System.out.println("Enter the Current Index of Task to Modify");
        int index = scanner.nextInt();


        if(index<0 || index>=Todolist.size()){
            System.out.println("Invalid Index");
            return;
        }

        Todolist.remove(index);
        System.out.println("Task at index " + index + " removed successfully.");

    }

    private static void ModifyTask() {
        System.out.println("Enter the Current Index of Task to Modify");
        int i = scanner.nextInt();
        if(i < Todolist.size()){
            Todolist.get(i).modify();
            System.out.println("Task at index " + i + " modified successfully.");
        }
        else{
            System.out.println("Invalid Index");
        }

    }

    private static void FetchTodolist() {
        for(int i=0;i< Todolist.size();i++){
            System.out.println(i + ". " + Todolist.get(i).getText() + " - " + Todolist.get(i).getStatus() + " - " + Todolist.get(i).getDate());
        }
    }

    private static void CreateTask() {
        System.out.println("Enter Task :");
        scanner.nextLine();
        String str = scanner.nextLine();
        Date date = new Date();
        Task newTask = new Task(str, 'p', date);
        Todolist.add(newTask);
        System.out.println("Task Added Successfully");
    }
}